If sometimes you just do something really cool or stupid, or you feel like being intimidating, and you need to emote that, this is the addon for you.

The sounds are by undead Hammerites from Thief: Shadow Project (Looking Glass Studio), bitcrushed by Pillow.